provider "aws" {
  region = "us-east-1"

}

module "nodes_sg" {
  source  = "./module/worker_node_sg"
  node_sg = var.sg_name[0]



}

module "master_sg" {
  source    = "./module/master_node_sg"
  master_sg = var.sg_name[1]



}
module "worker_node1" {
  source            = "./module/server"
  key_name          = var.key_name
  avail_zone        = var.avail_zone[0]
  instance_type     = var.instance_type
  subnet_ids        = var.subnet_ids[0]
  security_group_id = [module.nodes_sg.worker_sg.id]
  instance_name     = var.instance_name[0]
  script            = var.script


}
module "worker_node2" {
  source            = "./module/server"
  key_name          = var.key_name
  avail_zone        = var.avail_zone[1]
  instance_type     = var.instance_type
  subnet_ids        = var.subnet_ids[1]
  security_group_id = [module.nodes_sg.worker_sg.id]
  instance_name     = var.instance_name[1]
  script            = var.script


}

module "master_node" {
  source            = "./module/server"
  key_name          = var.key_name
  avail_zone        = var.avail_zone[2]
  instance_type     = var.instance_type
  subnet_ids        = var.subnet_ids[2]
  security_group_id = [module.master_sg.master_sg.id]
  instance_name     = var.instance_name[2]
  script            = var.script


}
output "master-public_ip" {
  value = module.master_node.server_ip.public_ip
}
output "master-private_ip" {
  value = module.master_node.server_ip.private_ip
}
output "worker_node1-public_ip" {
  value = module.worker_node1.server_ip.public_ip
}
output "worker_node1-private_ip" {
  value = module.worker_node1.server_ip.private_ip
}
output "worker_node2-public_ip" {
  value = module.worker_node2.server_ip.public_ip
}
output "worker_node2-private_ip" {
  value = module.worker_node2.server_ip.private_ip
}
