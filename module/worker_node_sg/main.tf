resource "aws_security_group" "nodes_sg" {
    name = var.node_sg
    description = "worker nodes security group"
    vpc_id = data.aws_vpc.app_vpc.id

  ingress {
    description      = "for ssh"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }
    ingress {
    description      = "for waeve-net communication"
    from_port        = 6783
    to_port          = 6783
    protocol         = "tcp"
    cidr_blocks      = [data.aws_vpc.app_vpc.cidr_block]
    
  }
  ingress {
    description      = "for NodePort servicest to be accessible from everywhere( for service)"
    from_port        = 30000
    to_port          = 32767
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }
 
    ingress {
    description      = "for Kubelet API to exchange with CP and himself  (must be scope to the cluster for no external traffic)"
    from_port        = 10250
    to_port          = 10250
    protocol         = "tcp"
    cidr_blocks      = [data.aws_vpc.app_vpc.cidr_block]
    
  }

    egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  
}

data "aws_vpc" "app_vpc" {
    filter {
      name = "tag:Name"
      values = ["kubernetes"]
    }

  
}