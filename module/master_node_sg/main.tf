resource "aws_security_group" "master_sg" {
    name = var.master_sg
    description = "master nodes security group"
    vpc_id = data.aws_vpc.app_vpc.id

  ingress {
    description      = "for ssh"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }
    ingress {
    description      = "for waeve-net communication"
    from_port        = 6783
    to_port          = 6783
    protocol         = "tcp"
    cidr_blocks      = [data.aws_vpc.app_vpc.cidr_block]
    
  }
  ingress {
    description      = "for all to communicate with Kubernetes API server"
    from_port        = 6443
    to_port          = 6443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }
    ingress {
    description      = "for ectd server client API to communicate with Kube-apiserver and etcd (must be scope to the cluster)"
    from_port        = 2379
    to_port          = 2380
    protocol         = "tcp"
    cidr_blocks      = [data.aws_vpc.app_vpc.cidr_block]
    
  }
    ingress {
    description      = "for Kubelet API to exchange with CP and himself && Kube-scheduler to exchange with himself (must be scope to the cluster)"
    from_port        = 10250
    to_port          = 10251
    protocol         = "tcp"
    cidr_blocks      = [data.aws_vpc.app_vpc.cidr_block]
    
  }

    egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  
}

data "aws_vpc" "app_vpc" {
    filter {
      name = "tag:Name"
      values = ["kubernetes"]
    }

  
}