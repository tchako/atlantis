
resource "aws_instance" "worker_nodes" { 
    ami = data.aws_ami.ubuntu.id
    key_name = var.key_name
    availability_zone = var.avail_zone
    instance_type = var.instance_type
    subnet_id = var.subnet_ids
    vpc_security_group_ids = var.security_group_id
    user_data = file(var.script)
    tags = {
        Name = var.instance_name
    }

  
}



data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}